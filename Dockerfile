FROM orchardup/python:3.4
ENV PYTHONUNBUFFERED 1
RUN apt-get update -qq && apt-get install -y python3-psycopg2 postgresql-client
RUN mkdir /code
WORKDIR /code
ADD . /code/
RUN pip3 install -r requirements/dev.txt
