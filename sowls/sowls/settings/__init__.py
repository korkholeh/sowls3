import os
mode = os.getenv('WSGI_ENV', 'dev')

if mode == 'dev':
    from .dev import *
elif mode == 'production':
    from .production import *
